# Шаблон для сборки фронтенда на основе [Symfony Webpack Encore](https://symfony.com/doc/current/frontend.html)

## Команды

- `yarn build` - Сборка для продакшена
- `yarn watch` - Запуск webpack с параметром --watch
- `yarn dev` - Запуск webpack-dev-server на порт 9000
- `yarn hot` - Запуск webpack-dev-server с поддержкой [Hot Module Replacement](https://webpack.js.org/guides/hot-module-replacement/)
