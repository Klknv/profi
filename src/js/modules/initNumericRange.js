function setDefaultValues(numeric) {
  const inpRanges = numeric.querySelectorAll('[type="range"]');
  const inpTexts = numeric.querySelectorAll('[type="text"]');

  if (+inpRanges[0].value > +inpRanges[1].value) {
    inpTexts[0].setAttribute('value', inpRanges[1].value + inpTexts[0].dataset.unit);
    inpTexts[1].setAttribute('value', inpRanges[0].value + inpTexts[1].dataset.unit);
  } else {
    inpTexts[0].setAttribute('value', inpRanges[0].value + inpTexts[0].dataset.unit);
    inpTexts[1].setAttribute('value', inpRanges[1].value + inpTexts[1].dataset.unit);
  }
}

function inputRangeInput() {
  const numeric = this.closest('.js-numeric-range');
  const inpRanges = numeric.querySelectorAll('[type="range"]');
  const inpTexts = numeric.querySelectorAll('[type="text"]');

  const firstInpValue = +inpRanges[0].value.replace(/\D+/g, '');
  const lastInpValue = +inpRanges[1].value.replace(/\D+/g, '');

  if (firstInpValue > lastInpValue) {
    inpTexts[0].value = lastInpValue + inpTexts[0].dataset.unit;
    inpTexts[1].value = firstInpValue + inpTexts[1].dataset.unit;
  } else {
    inpTexts[0].value = firstInpValue + inpTexts[0].dataset.unit;
    inpTexts[1].value = lastInpValue + inpTexts[1].dataset.unit;
  }
}

function inputTextsFocus() {
  this.value = (+this.value.replace(/\D+/g, '')).toLocaleString();
}

function inputTextsBlur() {
  this.value = this.value.replace(/\D+/g, '');
  if (this.dataset.unit) this.value += this.dataset.unit;
}

function inputTextInput() {
  this.value = (+this.value.replace(/\D+/g, '')).toLocaleString();
}

function inputTextChange() {
  const numeric = this.closest('.js-numeric-range');
  const inpRanges = numeric.querySelectorAll('[type="range"]');
  const inpTexts = numeric.querySelectorAll('[type="text"]');
  const maxValue = +inpRanges[0].max;

  let firstInpValue = +inpTexts[0].value.replace(/\D+/g, '');
  let lastInpValue = +inpTexts[1].value.replace(/\D+/g, '');

  if (firstInpValue > lastInpValue) {
    firstInpValue = (firstInpValue > maxValue) ? maxValue : firstInpValue;
    inpTexts[0].value = lastInpValue + inpTexts[0].dataset.unit;
    inpTexts[1].value = firstInpValue + inpTexts[1].dataset.unit;
    inpRanges[0].value = lastInpValue;
    inpRanges[1].value = firstInpValue;
  } else {
    lastInpValue = (lastInpValue > maxValue) ? maxValue : lastInpValue;
    inpTexts[0].value = firstInpValue + inpTexts[0].dataset.unit;
    inpTexts[1].value = lastInpValue + inpTexts[1].dataset.unit;
    inpRanges[0].value = firstInpValue;
    inpRanges[1].value = lastInpValue;
  }

  inpRanges[0].dispatchEvent(new Event('input', { bubbles: true }));
}

function initNumericRange() {
  const numerics = document.querySelectorAll('.js-numeric-range');
  if (!numerics.length) return;

  Array.prototype.forEach.call(numerics, (numeric) => {
    const inputRanges = numeric.querySelectorAll('[type="range"]');
    const inputTexts = numeric.querySelectorAll('[type="text"]');

    setDefaultValues(numeric);

    Array.prototype.forEach.call(inputRanges, (input) => {
      input.addEventListener('input', inputRangeInput);
    });

    Array.prototype.forEach.call(inputTexts, (input) => {
      input.addEventListener('focus', inputTextsFocus);
      input.addEventListener('blur', inputTextsBlur);
      input.addEventListener('input', inputTextInput);
      input.addEventListener('change', inputTextChange);
    });
  });
}


export default initNumericRange;
