function clickOutside(e) {
  const navLink = e.target.closest('.js-nav .nav__link');
  const navSubmenu = e.target.closest('.js-nav .nav__submenu');
  if (!navLink && !navSubmenu) {
    const items = document.querySelectorAll('.nav__list-item--show-menu');
    Array.prototype.forEach.call(items, (item) => {
      item.classList.remove('nav__list-item--show-menu');
    });
    document.removeEventListener('click', clickOutside);
  }
}

function toggleSubmenu(e) {
  const navLink = e.target.closest('.nav__link');
  if (!navLink) return;

  const isActive = navLink.closest('.nav__list-item--show-menu');
  const items = document.querySelectorAll('.nav__list-item--show-menu');
  Array.prototype.forEach.call(items, (item) => {
    item.classList.remove('nav__list-item--show-menu');
  });

  if (!navLink.nextElementSibling) return;

  e.preventDefault();

  if (!isActive) {
    navLink.parentElement.classList.add('nav__list-item--show-menu');
    document.addEventListener('click', clickOutside);
  }
}

function initNav() {
  const nav = document.querySelector('.js-nav');
  if (!nav) return;

  nav.addEventListener('click', toggleSubmenu);
}


export default initNav;
