const drawers = document.querySelectorAll('.js-drawer');
const openers = document.querySelectorAll('.js-open-drawer');
let tempWidthWindow = window.innerWidth;
let timer;

// Скрывает скролл
function hideScroll() {
  const { isMobile } = window;
  const isScroll = document.body.scrollHeight > window.innerHeight;

  if (!isMobile && isScroll) {
    const style = document.createElement('style');
    const scrollWidth = window.innerWidth - document.documentElement.clientWidth;
    const cssStyles = `.compensate-scrollbar{margin-right: ${scrollWidth}px;}`;

    const styleLockScroll = document.getElementById('style-lock-scroll');
    if (!styleLockScroll) {
      style.id = 'style-lock-scroll';
      style.appendChild(document.createTextNode(cssStyles));
      document.head.appendChild(style);
    }
  }

  document.body.classList.add('compensate-scrollbar');
  document.body.classList.add('page--no-scroll');
}

// Отображает скролл
function showScroll() {
  timer = setTimeout(() => {
    const styleLockScroll = document.getElementById('style-lock-scroll');
    if (styleLockScroll) styleLockScroll.remove();
    document.body.classList.remove('compensate-scrollbar');
    document.body.classList.remove('page--no-scroll');
  }, 333);
}

// Скрывает меню
function closeDrawer(drawer) {
  drawer.classList.remove('drawer--opened');
  Array.prototype.forEach.call(openers, (opener) => {
    if (opener.classList.contains('hamburger')) {
      opener.classList.remove('is-active');
    }
  });
  clearTimeout(timer);
  showScroll();
}

// Закрывает меню при ресайзе по ширине
function closeDrawerOnResize() {
  if (window.innerWidth !== tempWidthWindow) {
    Array.prototype.forEach.call(drawers, (drawer) => {
      closeDrawer(drawer);
    });
    window.removeEventListener('resize', closeDrawerOnResize);
  }
}

// Отображает меню
function openDrawer(drawer) {
  drawer.classList.add('drawer--opened');
  const hamburgers = document.querySelectorAll(`.hamburger[data-src="#${drawer.id}"]`);

  Array.prototype.forEach.call(hamburgers, (hamburger) => {
    hamburger.classList.add('is-active');
  });

  hideScroll();

  tempWidthWindow = window.innerWidth;
  window.addEventListener('resize', closeDrawerOnResize);
}

// Переключает отображение меню
function toggleShowDrawer(e) {
  const { src } = e.target.closest('.js-open-drawer').dataset;
  const drawer = document.querySelector(src);

  if (drawer.classList.contains('drawer--opened')) {
    closeDrawer(drawer);
    window.removeEventListener('resize', closeDrawerOnResize);
  } else {
    openDrawer(drawer);
  }
}

// Инициализирует меню
function initDrawer() {
  if (!drawers.length) return;

  Array.prototype.forEach.call(openers, (opener) => {
    opener.addEventListener('click', toggleShowDrawer);
  });

  document.addEventListener('click', (e) => {
    if (!e.target.closest('.drawer__body') && !e.target.closest('.js-open-drawer')) {
      Array.prototype.forEach.call(drawers, (drawer) => {
        closeDrawer(drawer);
        window.removeEventListener('resize', closeDrawerOnResize);
      });
    }
  });
}


export default initDrawer;
