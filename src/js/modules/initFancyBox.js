import '@fancyapps/fancybox';

function initFancyBox() {
  $.extend(true, $.fancybox.defaults, {
    btnTpl: {
      smallBtn:
        `<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">
          <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13 5L5 13" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M5 5L13 13" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
        </button>`,
    },
    lang: 'ru',
    i18n: {
      ru: {
        CLOSE: 'Закрыть',
        NEXT: 'Вперед',
        PREV: 'Назад',
        ERROR: 'Запрашиваемый контент не может быть загружен. <br/> Пожалуйста, попробуйте позже.',
        PLAY_START: 'Начать демонстрацию',
        PLAY_STOP: 'Приостановить демонстрацию',
        FULL_SCREEN: 'На полный экран',
        THUMBS: 'Миниатюры',
        DOWNLOAD: 'Скачать',
        SHARE: 'Поделиться',
        ZOOM: 'Увеличить',
      },
    },
  });

  $('[data-fancybox-form]').fancybox({
    buttons: [],
    touch: false,
  });
}


export default initFancyBox;
