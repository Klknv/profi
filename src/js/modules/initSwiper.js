import Swiper from 'swiper';

const swipersInstance = {};

function initDefault() {
  const swiper = document.querySelector('[data-swiper="default"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.default = new Swiper(swiperContainer, {
    slidesPerView: 2,
    spaceBetween: 20,
    roundLengths: true,
    breakpoints: {
      1200: {
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initIntro() {
  const swiper = document.querySelector('[data-swiper="intro"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.intro = new Swiper(swiperContainer, {
    slidesPerView: 2,
    spaceBetween: 20,
    roundLengths: true,
    autoHeight: true,
    effect: 'fade',
    crossFade: true,
    breakpoints: {
      1200: {
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initNews() {
  const swiper = document.querySelector('[data-swiper="news"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.intro = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 30,
    roundLengths: true,
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    breakpoints: {
      992: {
        pagination: {
          dynamicBullets: false,
        },
      },
    },
  });
}

function initProduction() {
  const swiper = document.querySelector('[data-swiper="production"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.intro = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 30,
    roundLengths: true,
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    breakpoints: {
      992: {
        pagination: {
          dynamicBullets: false,
        },
      },
    },
  });
}

function initReviews() {
  const swiper = document.querySelector('[data-swiper="reviews"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.services = new Swiper(swiperContainer, {
    spaceBetween: 30,
    roundLengths: true,
    breakpoints: {
      992: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 2,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initServices() {
  const swiper = document.querySelector('[data-swiper="services"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.services = new Swiper(swiperContainer, {
    spaceBetween: 30,
    roundLengths: true,
    breakpoints: {
      992: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 2,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initServicesShort() {
  const swiper = document.querySelector('[data-swiper="services-short"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance['services-short'] = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 12,
    roundLengths: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 2,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initReleases() {
  const swiper = document.querySelector('[data-swiper="releases"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.releases = new Swiper(swiperContainer, {
    spaceBetween: 12,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initReleasesShort() {
  const swiper = document.querySelector('[data-swiper="releases-short"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance['releases-short'] = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 12,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 2,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initProducts() {
  const swiper = document.querySelector('[data-swiper="products"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.products = new Swiper(swiperContainer, {
    spaceBetween: 12,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initProductsShort() {
  const swiper = document.querySelector('[data-swiper="products-short"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance['products-short'] = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 12,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 2,
        pagination: {
          dynamicBullets: false,
        },
      },
    },
    pagination: {
      el: swiper.querySelector('.swiper-pagination'),
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initPartner() {
  const swiper = document.querySelector('[data-swiper="partner"]');
  if (!swiper) return;

  const swiperContainer = swiper.querySelector('.swiper-container');
  swipersInstance.partner = new Swiper(swiperContainer, {
    spaceBetween: 12,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    breakpoints: {
      450: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 3,
      },
    },
    navigation: {
      nextEl: swiper.querySelector('.swiper-button-next'),
      prevEl: swiper.querySelector('.swiper-button-prev'),
    },
  });
}

function initGallery() {
  const swipers = document.querySelectorAll('.js-swiper-gallery .section-swiper');
  if (!swipers.length) return;

  const galleryThumbs = new Swiper(swipers[1].querySelector('.swiper-container'), {
    spaceBetween: 18,
    slidesPerView: 2,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      400: {
        slidesPerView: 3,
      },
      450: {
        slidesPerView: 3,
        spaceBetween: 25,
      },
    },
  });

  // eslint-disable-next-line no-unused-vars
  const galleryTop = new Swiper(swipers[0].querySelector('.swiper-container'), {
    spaceBetween: 25,
    roundLengths: true,
    navigation: {
      nextEl: swipers[1].querySelector('.swiper-button-next'),
      prevEl: swipers[1].querySelector('.swiper-button-prev'),
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });
}

function initGalleryDetail() {
  const swipers = document.querySelectorAll('.js-swiper-gallery-detail .section-swiper');
  if (!swipers.length) return;

  const galleryThumbs = new Swiper(swipers[1].querySelector('.swiper-container'), {
    spaceBetween: 13,
    slidesPerView: 2,
    roundLengths: true,
    freeMode: true,
    freeModeSticky: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      350: {
        slidesPerView: 3,
      },
      450: {
        slidesPerView: 4,
      },
      576: {
        slidesPerView: 5,
      },
      768: {
        slidesPerView: 7,
      },
    },
  });

  // eslint-disable-next-line no-unused-vars
  const galleryTop = new Swiper(swipers[0].querySelector('.swiper-container'), {
    spaceBetween: 25,
    roundLengths: true,
    navigation: {
      nextEl: swipers[1].querySelector('.swiper-button-next'),
      prevEl: swipers[1].querySelector('.swiper-button-prev'),
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });
}

function initSwiper() {
  initDefault();
  initIntro();
  initNews();
  initProduction();
  initReviews();
  initServices();
  initServicesShort();
  initReleases();
  initReleasesShort();
  initProducts();
  initProductsShort();
  initPartner();
  initGallery();
  initGalleryDetail();
}


export default initSwiper;
