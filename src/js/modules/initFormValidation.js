import 'jquery-validation/dist/jquery.validate.min';
import 'jquery-validation/dist/localization/messages_ru';


function showSuccess() {
  const opts = {
    buttons: [],
    touch: false,
  };

  const instanceTemp = $.fancybox.getInstance();
  if (instanceTemp) opts.beforeShow = () => instanceTemp.close();

  $.fancybox.open({ src: '#successful-send', opts });
}


function submitFormCalculator(form) {
  const calculator = document.querySelector('.js-calculator');
  const activeTabbedButtons = calculator.querySelectorAll('.tabbed__list-item:not(.d-none) .tabbed__button');

  const panels = Array.prototype.map.call(activeTabbedButtons,
    (button) => calculator.querySelector(button.hash));

  const data = Array.prototype.map.call(panels, (panel) => {
    const controls = panel.querySelectorAll('input, select, textarea');
    return $(controls).serializeArray();
  });

  // eslint-disable-next-line no-console
  console.log('Отправка формы калькулятора!!');
  // eslint-disable-next-line no-console
  console.log(data);

  form.reset();
  showSuccess();
}

function submitForm(form) {
  // eslint-disable-next-line no-console
  console.log('Отправка формы!!');

  form.reset();
  showSuccess();
}

function submitHandlerForms(form) {
  if (form.id === 'form-calculator') {
    submitFormCalculator(form);
    return;
  }

  submitForm(form);
}

function validateForm(form) {
  $(form).validate({
    errorClass: 'input-validation input-validation--invalid',
    validClass: 'input-validation input-validation--valid',
    errorElement: 'span',
    errorPlacement: ($error, $element) => {
      $element.closest('.input, .checkbox')
        .find('.input-validation__message')
        .html($error);
    },
    normalizer: (value) => $.trim(value),
    rules: {
      agreement: { required: true },
      name: { required: true, minlength: 3 },
      company_name: { required: true, minlength: 3 },
      company_size: { required: true, minlength: 3 },
      contact_person: { required: true, minlength: 3 },
      email: { email: true },
      phone: {
        normalizer: (value) => value.replace(/\D+/g, ''),
        required: true,
        minlength: 11,
      },
    },
    messages: {
      agreement: 'Укажите согласие c правилами пользования',
    },
    submitHandler: submitHandlerForms,
  });
}

function initFormValidation() {
  $('.js-form-validation').each((i, form) => {
    validateForm(form);
  });
}

export default initFormValidation;
