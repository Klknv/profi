function setCheckedRadio(e) {
  const targetInput = e.target.closest('.js-input-in-radio');
  if (!targetInput) return;

  const radioControl = targetInput.querySelector('[type="radio"]');
  radioControl.checked = true;

  const event = new Event('input', {
    bubbles: true,
    cancelable: true,
  });
  radioControl.dispatchEvent(event);
}

function setValueRadio(e) {
  const container = e.target.closest('.js-input-in-radio');
  const radio = container.querySelector('input[type="radio"]');
  radio.value = e.target.value;
}

function initInputInRadio() {
  document.addEventListener('focus', setCheckedRadio, true);

  const inputs = document.querySelectorAll('.js-input-in-radio');
  Array.prototype.forEach.call(inputs, (input) => {
    const inpText = input.querySelector('input[type="text"]');
    inpText.addEventListener('input', setValueRadio);
  });
}


export default initInputInRadio;
