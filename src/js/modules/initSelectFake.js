function setValueSelect() {
  const valueContainer = this.nextElementSibling;
  valueContainer.textContent = this.value;
}

function initSelectFake() {
  const selects = document.querySelectorAll('.js-select-fake');
  if (!selects) return;

  Array.prototype.forEach.call(selects, (select) => {
    const control = select.querySelector('.select__control');
    control.addEventListener('input', setValueSelect);
  });
}


export default initSelectFake;
