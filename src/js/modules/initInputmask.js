import Inputmask from 'inputmask';

function initInputmask() {
  document.addEventListener('click', (event) => {
    const phone = event.target.closest('input[name="phone"]');

    if (!phone || !document.contains(phone)) return;

    const im = new Inputmask('+7 (999) 999-99-99');
    im.mask(phone);
  });
}

export default initInputmask;
