function initTabbed() {
  // Get relevant elements and collections
  const tabbeds = document.querySelectorAll('.tabbed');
  if (!tabbeds.length) return;

  Array.prototype.forEach.call(tabbeds, (tabbed) => {
    const tablist = tabbed.querySelector('.tabbed__list');
    const tabs = tablist.querySelectorAll('.tabbed__button');
    const panels = tabbed.querySelectorAll(':scope > .tabbed__section, .tabbed__sections > .tabbed__section');

    // The tab switching function
    const switchTab = (oldTab, newTab) => {
      newTab.focus();
      // Make the active tab focusable by the user (Tab key)
      newTab.removeAttribute('tabindex');
      // Set the selected state
      newTab.setAttribute('aria-selected', 'true');
      oldTab.removeAttribute('aria-selected');
      oldTab.setAttribute('tabindex', '-1');
      // Get the indices of the new and old tabs to find the correct
      // tab panels to show and hide
      const oldPanel = tabbed.querySelector(oldTab.hash);
      const newPanel = tabbed.querySelector(newTab.hash);
      oldPanel.hidden = true;
      newPanel.hidden = false;
    };

    // Add the tablist role to the first <ul> in the .tabbed container
    tablist.setAttribute('role', 'tablist');

    // Add semantics are remove user focusability for each tab
    Array.prototype.forEach.call(tabs, (tab, i) => {
      tab.setAttribute('role', 'tab');
      tab.setAttribute('id', `tab${i + 1}`);
      tab.setAttribute('tabindex', '-1');
      tab.parentNode.setAttribute('role', 'presentation');

      // Handle clicking of tabs for mouse users
      tab.addEventListener('click', (e) => {
        e.preventDefault();
        const currentTab = tablist.querySelector('[aria-selected]');
        if (e.currentTarget !== currentTab) {
          switchTab(currentTab, e.currentTarget);
        }
      });

      // Handle keydown events for keyboard users
      tab.addEventListener('keydown', (e) => {
        // Get the index of the current tab in the tabs node list
        const index = Array.prototype.indexOf.call(tabs, e.currentTarget);
        // Work out which key the user is pressing and
        // Calculate the new tab's index where appropriate
        if (e.keyCode === 37 && tabs[index - 1]) {
          e.preventDefault();
          switchTab(e.currentTarget, tabs[index - 1]);
          return;
        }
        if (e.keyCode === 39 && tabs[index + 1]) {
          e.preventDefault();
          switchTab(e.currentTarget, tabs[index + 1]);
          return;
        }
        if (e.keyCode === 40) {
          e.preventDefault();
          panels[i].focus();
        }
      });
    });

    // Add tab panel semantics and hide them all
    Array.prototype.forEach.call(panels, (pnl, i) => {
      const panel = pnl;
      panel.setAttribute('role', 'tabpanel');
      panel.setAttribute('tabindex', '-1');
      panel.setAttribute('aria-labelledby', tabs[i].id);
      panel.hidden = true;
    });

    // Initially activate the first tab and reveal the first tab panel
    tabs[0].removeAttribute('tabindex');
    tabs[0].setAttribute('aria-selected', 'true');
    panels[0].hidden = false;
  });
}


export default initTabbed;
