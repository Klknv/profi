const calculator = document.querySelector('.js-calculator');
const calculatorTable = document.querySelector('.js-characteristic-table');

function showTabbeds(id) {
  const tabbeds = document.querySelectorAll('[data-show-for]');

  Array.prototype.forEach.call(tabbeds, (tabbed) => {
    const { showFor } = tabbed.dataset;
    let isFound = false;

    Array.prototype.forEach.call(showFor, (showId) => {
      if (+showId === +id) {
        isFound = true;
        tabbed.classList.remove('d-none');
      }
    });

    if (!isFound) {
      tabbed.classList.add('d-none');
    }
  });
}

function changeSlides() {
  const changeInput = document.querySelector('.js-change-slides');
  showTabbeds(changeInput.selectedOptions[0].dataset.showId);

  document.addEventListener('change', (e) => {
    const control = e.target.closest('.js-change-slides');
    if (!control) return;

    const { showId } = control.selectedOptions[0].dataset;
    showTabbeds(showId);
  });
}

function prevSlide() {
  const tabbedButtons = calculator.querySelectorAll('.tabbed__list-item:not(.d-none) .tabbed__button');
  const activeButton = calculator.querySelector('[aria-selected="true"]');
  const indexActivTab = Array.prototype.indexOf.call(tabbedButtons, activeButton);

  if (tabbedButtons[indexActivTab - 1]) {
    tabbedButtons[indexActivTab - 1].click();
  }
}

function nextSlide() {
  const tabbedButtons = calculator.querySelectorAll('.tabbed__list-item:not(.d-none) .tabbed__button');
  const activeButton = calculator.querySelector('[aria-selected="true"]');
  const indexActivTab = Array.prototype.indexOf.call(tabbedButtons, activeButton);

  if (tabbedButtons[indexActivTab + 1]) {
    tabbedButtons[indexActivTab + 1].click();
  }
}

function disabledInputs(inputs) {
  Array.prototype.forEach.call(inputs, (input) => {
    // eslint-disable-next-line no-param-reassign
    input.disabled = true;
  });
}

function enabledInputs(inputs) {
  Array.prototype.forEach.call(inputs, (input) => {
    // eslint-disable-next-line no-param-reassign
    input.disabled = false;
  });
}

function toggleSize(e) {
  const tabbedSection = e.target.closest('.tabbed__section');
  const calcSizes = tabbedSection.querySelector('.calculator__sizes');
  const inputs = calcSizes.querySelectorAll('input');
  const totalSizesInput = tabbedSection.querySelectorAll('.js-total-sizes');
  if (e.target.checked) {
    calcSizes.classList.remove('d-none');
    enabledInputs(inputs);
    disabledInputs(totalSizesInput);
  } else {
    calcSizes.classList.add('d-none');
    disabledInputs(inputs);
    enabledInputs(totalSizesInput);
  }
}

function toggleMaterial(e) {
  const tabbedSection = e.target.closest('.tabbed__section');
  const calcMaterials = tabbedSection.querySelector('.calculator__material');
  const inputs = calcMaterials.querySelectorAll('input');

  if (e.target.classList.contains('js-target-toggle')) {
    calcMaterials.classList.add('d-none');
    disabledInputs(inputs);
  } else {
    calcMaterials.classList.remove('d-none');
    enabledInputs(inputs);
  }
}

function drawTable(data) {
  Array.prototype.forEach.call(data, (characteristic) => {
    const tr = document.createElement('tr');
    const tdName = document.createElement('td');
    const tdValue = document.createElement('td');
    const chrtrNameContainer = document.createElement('div');
    const chrtrValueContainer = document.createElement('div');
    const createChrtrName = (text) => {
      const el = document.createElement('div');
      el.classList.add('characteristic__name');
      el.classList.add('characteristic__name--sm');
      el.innerHTML = text;
      return el;
    };
    const createChrtrValue = (text) => {
      const el = document.createElement('div');
      el.classList.add('characteristic__value');
      el.innerHTML = text;
      return el;
    };
    let isCharacteristic = false;

    tr.id = `chr_${characteristic.name}`;

    if (characteristic.name === 'vid_obyekta') {
      chrtrNameContainer.append(createChrtrName('Объект'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'etazhnost') {
      const chrValue = document.querySelector('#chr_vid_obyekta .characteristic__value');
      if (!chrValue) return;
      const span = document.createElement('span');
      span.classList.add('characteristic__postfix');
      span.innerHTML = ` (${characteristic.value} этаж)`;
      chrValue.append(span);
    }

    if (characteristic.name === 'obshchaya_ploshchad') {
      const etazhnost = +calculator.querySelector('[name="etazhnost"]').value;
      chrtrNameContainer.append(createChrtrName('Площадь'));
      chrtrValueContainer.append(createChrtrValue(`${characteristic.value * etazhnost} м2`));
      isCharacteristic = true;
    }

    if (characteristic.name === 'dlina') {
      const ploshchad = calculator.querySelector('[name="obshchaya_ploshchad"]');
      const dlina = calculator.querySelector('[name="dlina"]').value;
      const shirina = calculator.querySelector('[name="shirina"]').value;
      const etazhnost = +calculator.querySelector('[name="etazhnost"]').value;

      const total = dlina * shirina * etazhnost;
      ploshchad.value = dlina * shirina;

      chrtrNameContainer.append(createChrtrName('Площадь'));
      chrtrValueContainer.append(createChrtrValue(`${total} м2`));
      isCharacteristic = true;
    }

    if (characteristic.name === 'tip_kryshi_1'
        || characteristic.name === 'tip_kryshi_2'
    ) {
      chrtrNameContainer.append(createChrtrName('Тип крыши'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'material'
        || characteristic.name === 'material_1'
    ) {
      const chrValue = document.querySelector('[id^="chr_tip_kryshi"] .characteristic__value');
      if (!chrValue) return;
      const span = document.createElement('span');
      span.classList.add('characteristic__postfix');
      span.innerHTML = ` (${characteristic.value})`;
      chrValue.append(span);
    }

    if (characteristic.name === 'tip_uteplitelya') {
      chrtrNameContainer.append(createChrtrName('Тип утеплителя'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'naruzhnaya_obshivka'
        || characteristic.name === 'naruzhnaya_obshivka_1'
        || characteristic.name === 'naruzhnaya_obshivka_2'
    ) {
      chrtrNameContainer.append(createChrtrName('Наружная обшивка стен'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'vnutrennyaya_oblitsovka'
      || characteristic.name === 'vnutrennyaya_oblitsovka_1'
      || characteristic.name === 'vnutrennyaya_oblitsovka_2'
      || characteristic.name === 'vnutrennyaya_oblitsovka_3'
    ) {
      chrtrNameContainer.append(createChrtrName('Внутренняя облицовка'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'fasadnaya_oblitsovka'
      || characteristic.name === 'fasadnaya_oblitsovka_1'
      || characteristic.name === 'fasadnaya_oblitsovka_2'
      || characteristic.name === 'fasadnaya_oblitsovka_3'
    ) {
      chrtrNameContainer.append(createChrtrName('Фасадная облицовка'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'tip_okna') {
      chrtrNameContainer.append(createChrtrName('Тип окна'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'tsvet_okon') {
      chrtrNameContainer.append(createChrtrName('Цвет окон'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (characteristic.name === 'tip_fundamenta') {
      chrtrNameContainer.append(createChrtrName('Тип фундамента'));
      chrtrValueContainer.append(createChrtrValue(characteristic.value));
      isCharacteristic = true;
    }

    if (isCharacteristic) {
      tdName.append(chrtrNameContainer);
      tdValue.append(chrtrValueContainer);
      tr.append(tdName);
      tr.append(tdValue);
      calculatorTable.append(tr);
    }
  });
}

function outputCharacteristic() {
  const activeTabbedButtons = calculator.querySelectorAll('.tabbed__list-item:not(.d-none) .tabbed__button');
  calculatorTable.innerHTML = '';

  Array.prototype.some.call(activeTabbedButtons, (button) => {
    const panel = calculator.querySelector(button.hash);
    const controls = panel.querySelectorAll('input, select');
    const data = $(controls).serializeArray();

    drawTable(data);

    return button.hasAttribute('aria-selected');
  });
}

function initCalculator() {
  if (!calculator) return;
  changeSlides();
  outputCharacteristic();

  const toggleSizesControl = document.querySelector('.js-toggle-size');
  toggleSizesControl.addEventListener('input', toggleSize);

  const toggleMaterialControls = document.querySelectorAll('fieldset.js-toggle-material input[type="radio"]');
  Array.prototype.forEach.call(toggleMaterialControls, (toggleMaterialControl) => {
    toggleMaterialControl.addEventListener('change', toggleMaterial);
  });

  document.addEventListener('click', (e) => {
    const buttonPrev = e.target.closest('[data-calculator-prev]:not(:disabled)');
    const buttonNext = e.target.closest('[data-calculator-next]:not(:disabled)');

    if (buttonPrev) prevSlide();
    if (buttonNext) nextSlide();
  });

  const controls = calculator.querySelectorAll('.tabbed__button, input, select');
  Array.prototype.forEach.call(controls, (control) => {
    if (control.classList.contains('tabbed__button')) {
      control.addEventListener('click', () => { setTimeout(outputCharacteristic); });
    } else {
      control.addEventListener('input', () => { setTimeout(outputCharacteristic); });
    }
  });
}


export default initCalculator;
