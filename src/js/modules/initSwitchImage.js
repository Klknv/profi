function initSwitchImage() {
  const switchContainers = document.querySelectorAll('.js-switch-image');

  Array.prototype.forEach.call(switchContainers, (container) => {
    container.addEventListener('click', (e) => {
      const button = e.target.closest('.accordion__button');
      if (!button) return;

      const switchUrl = button.dataset.urlSwitch;
      const switchContainer = button.closest('.js-switch-image');

      const imgContainer = switchContainer.querySelector('[data-switch-container]');
      if (!imgContainer) return;

      imgContainer.src = switchUrl;
    });
  });
}


export default initSwitchImage;
