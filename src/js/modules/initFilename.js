function processSelectedFiles(e) {
  const input = e.target;
  const inputContainer = input.closest('.input');
  const filename = inputContainer.querySelector('.input__filename');

  Array.prototype.forEach.call(input.files, (file) => {
    filename.innerHTML = `<div class="pt-1">${file.name}</div>`;
  });
}


function initFilename() {
  const inputs = document.querySelectorAll('.js-filename [type="file"]');

  Array.prototype.forEach.call(inputs, (input) => {
    input.addEventListener('input', processSelectedFiles);
  });
}


export default initFilename;
