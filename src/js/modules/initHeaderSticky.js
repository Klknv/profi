const header = document.querySelector('header.header');

function applySticky() {
  if (window.scrollY >= header.clientHeight) {
    header.classList.add('header--sticky');
  } else {
    header.classList.remove('header--sticky');
  }
}

function handlerMatchMedia(match) {
  if (match.matches) {
    window.addEventListener('scroll', applySticky);
  } else {
    window.removeEventListener('scroll', applySticky);
  }
}

function initHeaderSticky() {
  const match = window.matchMedia('(min-width: 1200px)');
  handlerMatchMedia(match);
  applySticky();

  try {
    // Chrome & Firefox
    match.addEventListener('change', handlerMatchMedia.bind(this, match));
  } catch (e1) {
    try {
      // Safari
      match.addListener(handlerMatchMedia.bind(this, match));
    } catch (e2) {
      // eslint-disable-next-line no-console
      console.error(e2);
    }
  }
}


export default initHeaderSticky;
