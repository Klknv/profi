function printElem(elem) {
  const mywindow = window.open('', 'PRINT', 'height=400,width=600');

  mywindow.document.write('<html lang="ru"><head><title>Яндекс.Карта - ООО «ИСК ПРОФИ»</title>');
  mywindow.document.write('<style>.map__control{min-width: 700px; width: 100%; height: 600px; border: 3px solid #ccc;}</style>');
  mywindow.document.write('</head><body>');
  mywindow.document.write(document.querySelector(elem).innerHTML);
  mywindow.document.write('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');
  mywindow.document.write('<script type="text/javascript">ymaps.ready(window.print)</script>');
  mywindow.document.write('</body></html>');

  mywindow.document.close(); // necessary for IE >= 10
  mywindow.focus(); // necessary for IE >= 10*/

  return true;
}

function initPrintElem() {
  const buttonsPrint = document.querySelectorAll('[data-print]');
  Array.prototype.forEach.call(buttonsPrint, (button) => {
    button.addEventListener('click', (e) => {
      const targetBtn = e.target.closest('[data-print]');
      printElem(targetBtn.dataset.print);
    });
  });
}


export default initPrintElem;
