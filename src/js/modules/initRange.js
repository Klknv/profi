function initRangeProgress() {
  const ranges = document.querySelectorAll('.js-range-progress');
  if (!ranges.length) return;

  const getProgressValue = (input) => {
    const inputContainer = input.closest('.js-range-progress');
    const inputs = inputContainer.querySelectorAll('input[type="range"]');
    const inputsValue = [];

    Array.prototype.forEach.call(inputs, (inp) => {
      inputsValue.push(+inp.value);
    });

    const min = +input.min;
    const max = +input.max;
    const inpLength = (max - min) || 100;

    const inputsRatio = Array.prototype.map.call(inputsValue, (value) => {
      const ratio = (value - min) / inpLength;
      return (ratio) * 100;
    });

    inputsRatio.sort((a, b) => a - b);

    return [
      inputsRatio.length === 1 ? 0 : inputsRatio[0],
      inputsRatio[inputsRatio.length - 1] || 0,
    ];
  };

  const setProgressValue = (input) => {
    const range = input.closest('.js-range-progress');
    range.style.setProperty('--js-range-progress-start', `${getProgressValue(input)[0]}%`);
    range.style.setProperty('--js-range-progress-end', `${getProgressValue(input)[1]}%`);
  };

  ranges.forEach((range) => {
    const controls = range.querySelectorAll('input[type="range"]');

    Array.prototype.forEach.call(controls, (control) => {
      setTimeout(() => setProgressValue(control));
      control.addEventListener('input', (e) => setProgressValue(e.target));
    });
  });
}

function initRange() {
  initRangeProgress();

  const inputReset = document.querySelectorAll('[type="reset"]');
  Array.prototype.forEach.call(inputReset, (input) => {
    const form = input.closest('form');
    form.addEventListener('reset', setTimeout.bind(this, () => {
      const ranges = form.querySelectorAll('[type="range"]');

      Array.prototype.forEach.call(ranges, (range) => {
        range.dispatchEvent(new Event('input', { bubbles: true }));
      });
    }));
  });
}


export default initRange;
