let loadingActivePanels = false;

function showPanel(pnl) {
  const panel = pnl;
  panel.previousElementSibling.classList.add('accordion__button--active');
  panel.style.height = `${panel.scrollHeight}px`;
}

function hidePanel(pnl) {
  const panel = pnl;
  panel.previousElementSibling.classList.remove('accordion__button--active');
  panel.style.height = `${panel.scrollHeight}px`;
  setTimeout(() => {
    panel.style.height = null;
  });
}

function toggleAccordion(e) {
  const button = e.target.closest('button');
  const panel = button.nextElementSibling;
  const isActive = button.classList.contains('accordion__button--active');

  if (isActive) {
    hidePanel(panel);
  } else {
    showPanel(panel);
  }
}

function toggleAccordionOnce(e) {
  const accordion = e.target.closest('.js-accordion-once');
  const activePanels = accordion.querySelectorAll('.accordion__button--active + .accordion__panel');

  toggleAccordion(e);
  Array.prototype.forEach.call(activePanels, hidePanel);
}

function initAccordion() {
  const accordions = document.querySelectorAll('.js-accordion, .js-accordion-once');
  if (!accordions.length) return;

  const panel = document.querySelector('.accordion__panel');
  if (panel) {
    panel.addEventListener('transitionend', (e) => {
      if (e.target.style.height) e.target.style.height = 'initial';
      if (!loadingActivePanels) {
        loadingActivePanels = true;
        const activePanels = document.querySelectorAll('.accordion__button--active + .accordion__panel');
        Array.prototype.forEach.call(activePanels, (pnl) => {
          // eslint-disable-next-line no-param-reassign
          pnl.style.height = 'initial';
        });
      }
    });
  }

  Array.prototype.forEach.call(accordions, (accordion) => {
    const activePanels = accordion.querySelectorAll('.accordion__button--active + .accordion__panel');
    Array.prototype.forEach.call(activePanels, (pnl) => {
      showPanel(pnl);
    });

    const buttons = accordion.querySelectorAll('.accordion__button');
    const isOnce = accordion.classList.contains('js-accordion-once');
    Array.prototype.forEach.call(buttons, (button) => {
      button.addEventListener('click', isOnce ? toggleAccordionOnce : toggleAccordion);
    });
  });
}


export default initAccordion;
