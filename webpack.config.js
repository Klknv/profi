const Encore = require('@symfony/webpack-encore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack');

Encore
  .setOutputPath('static/')
  .setPublicPath('/')
  .addEntry('app.min', './src/js/app.js')
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/index.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/components.html',
    filename: 'components.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/catalog-box.html',
    filename: 'catalog-box.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/catalog-list.html',
    filename: 'catalog-list.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/news-list.html',
    filename: 'news-list.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/info-list.html',
    filename: 'info-list.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/calculator.html',
    filename: 'calculator.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/how-to-order.html',
    filename: 'how-to-order.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/delivery.html',
    filename: 'delivery.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/warranty.html',
    filename: 'warranty.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/partners.html',
    filename: 'partners.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/for-dealers.html',
    filename: 'for-dealers.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/services.html',
    filename: 'services.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/detail-article.html',
    filename: 'detail-article.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/product-catalog.html',
    filename: 'product-catalog.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/reviews.html',
    filename: 'reviews.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/faq.html',
    filename: 'faq.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/main.html',
    filename: 'main.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/documents.html',
    filename: 'documents.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/design.html',
    filename: 'design.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/building.html',
    filename: 'building.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/building-2.html',
    filename: 'building-2.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/about.html',
    filename: 'about.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/contacts.html',
    filename: 'contacts.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/cooperation.html',
    filename: 'cooperation.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/product-catalog-2.html',
    filename: 'product-catalog-2.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/detail-product.html',
    filename: 'detail-product.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/technology.html',
    filename: 'technology.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/detail-news.html',
    filename: 'detail-news.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/production.html',
    filename: 'production.html',
  }))
  .addPlugin(new HtmlWebpackPlugin({
    template: 'src/templates/all-categories.html',
    filename: 'all-categories.html',
  }));

Encore
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableSassLoader()
  .enableEslintLoader()
  .enablePostCssLoader()
  .autoProvidejQuery()
  .configureUrlLoader({
    images: {
      limit: 8192,
    },
    fonts: {
      limit: 4096,
    },
  })
  .configureFilenames({
    images: 'img/export/[name].[hash:8].[ext]',
  })
  .copyFiles({
    from: './src/img',
    to: 'img/[path][name].[ext]',
    pattern: /\.(png|jpe?g|gif|svg|webp)$/,
  })
  .copyFiles({
    from: './src/favicon',
    to: 'favicon/[path][name].[ext]',
  })
  .addPlugin(new FriendlyErrorsWebpackPlugin())
  .addPlugin(new ImageminPlugin({
    bail: false,
    cache: true,
    imageminOptions: {
      plugins: [
        ['gifsicle', { interlaced: true }],
        ['mozjpeg', { progressive: true, quality: 85 }],
        ['pngquant', { quality: [0.6, 0.83], speed: 1 }],
        ['svgo', {
          plugins: [
            { removeViewBox: false },
            { removeUnknownsAndDefaults: { unknownAttrs: false } },
            { cleanupIDs: false },
            { collapseGroups: false },
          ],
        }],
      ],
    },
  }));

module.exports = Encore.getWebpackConfig();
